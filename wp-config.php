<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3KJdNKsWp7GBfVDAVG9enOp+FpSedeTYQf9RMwpWJLzOm9UirwNos1Vsd7xzh+6V51kXQKtvEM8/8ShN7GV+EQ==');
define('SECURE_AUTH_KEY',  'vVTs4FqvpbF38F4Zdya7YZ6pHcQ6FOc8gLsONZQfI2UtlCxoYyZBFX3WNDq8zQK0EVrOSW0bMzakzKxewUn51A==');
define('LOGGED_IN_KEY',    'qAdX8mwfi9j0WYbkw9nuZC0CdIGVmxrotcamsCwGJXbUzj4tRtIsIpydNh4zLTAvi9Y4gnMElnYsntuhG/l2Gw==');
define('NONCE_KEY',        'Q1PyoiTyE74OXp9ulssN6vNuyMUhnpWgc8yyzv9pgNc5pyL/Y+tOPE3YALNbaHt9Uo3pb8TwgyErMMjqVQEr7A==');
define('AUTH_SALT',        'G8hgcFte0T9luol6JKwBtmiJsZ904cDNzZ1JnbQZITi5V5+sUYQS5rMF0PFkX/1i1NBIykyaXlTfc6v34hNvtA==');
define('SECURE_AUTH_SALT', 'dK/1rnqUNYSV7zcbSLETmzaJ/d/XtcQXJUt+GnsswXquX395w0PNOvkSTOvB+ti7ZOXjKNlrTY8l5ASOLfQXvA==');
define('LOGGED_IN_SALT',   'ocYKVg06EAiKs5eXUZAyO6eH6n0Vg8F34h8VvRyS48b3tvzoJ90wp2rB12myhJU7FAoHSC/IHdi+VUPzXl11ZA==');
define('NONCE_SALT',       'RI0onH1oKxbm4/0Fb59kE+LOuyvEEGcC4r6gRQt7FsEIhfAuygO0Hht9HnhXntPhsU8Ox5QdBWmhCr8RByuv+g==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_qbglbmoyfp_';





/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* Inserted by Local by Flywheel. Fixes $is_nginx global for rewrites. */
if (strpos($_SERVER['SERVER_SOFTWARE'], 'Flywheel/') !== false) {
	$_SERVER['SERVER_SOFTWARE'] = 'nginx/1.10.1';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
